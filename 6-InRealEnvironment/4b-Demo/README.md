# How to use Docker for this example?

First of all, make sure [Docker](https://www.docker.com/) is properly installed and running.

## Getting the image

Then to get the relevant image you may either:

### Build the image from the provided Dockerfile

Make sure to have first built the image in Config directory (at the root of the project).

Then type in a terminal:

````
docker build -t demo_third_party_warning .
````

This may take few minutes.

### Or fetching it from the gitlab registry

Type in a terminal (provided you have an account on the [Inria gitlab](https://gitlab.inria.fr) - if not you may create one easily):

````
docker login registry.gitlab.inria.fr
docker pull registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/demo_third_party_warning:latest
````

## Running the image

Go in the _Docker/ThirdPartyWarning_ folder and type:

````
docker run -v $PWD:/Codes/ThirdPartyWarning --cap-drop=all -it demo_third_party_warning
````

For those of you not familiar with Docker:

* `-v` creates a mapping between local folder and the /Codes/ThirdPartyWarning folder in the container; this enables you to edit the file from your comfy environment and see the file edited this way in the Docker container.
* `--cap-drop=all` is a safety when you're running a Docker image not built by yourself: you're essentially blocking the few remaining operations that might impact your own environment that Docker lets by default open with the run command.
* `-it` tells we want an interactive session: your terminal session will place you inside the container (here a Fedora environment).

`third_party_warning` is a Docker **image**; the instantiation of this image obtained after the run command is a **container**.


# Step by step: illustrating the issue

After run, the container is opened and you are in a folder (inside the container) named `/Codes/ThirdPartyWarning`.

**NOTE** All the commands listed below must be run _INSIDE_ the container.

## Illustrating the issue

Go in the first directory:

````
cd 1-ProblematicCase/
````

You will see there the `simple_boost.cpp` presented in the notebook, and a CMakeLists.txt to compile it with CMake.

Please proceed:

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

At the time of this writing, I get 1355 warnings! (more than in the first run one year ago with Boost 1.69...)

## First solution: system directories

Go in the second directory:

````
cd ../../2-FixByBuildSystem
````

The only change is in the CMakeLists.txt file: 

````
target_include_directories(simple_boost PUBLIC "/Codes/ThirdParty/opt/include")
````

is replaced by:

````
target_include_directories(simple_boost SYSTEM PUBLIC "/Codes/ThirdParty/opt/include")
````

And now the compilation works without warnings:

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

## Second solution: pragmas

### Fixing clang warnings

Go in the third directory:

````
cd ../../3-FixByPragmas-clang
````

This time, `CMakeLists.txt` is exactly the same as in `1-ProblematicCase` but the `simple_boost.cpp` is amended: includes are surrounded by clang pragmas to deactivate warnings.

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

and we're all right! At least if we stick with clang... Let's compile with gcc to see:

````
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

and we get two type of warnings:

* -Wsign-conversion that were present in clang and fixed by the pragmas
* -Wunknown-pragmas as pragmas used for clang build are not recognized! So fixing for clang makes it somehwat worse for gcc.. (not completely - some ignored warnings do also exist in gcc and were therefore silenced).

### Fixing clang AND gcc warnings

Go in the fourth directory:

````
cd ../../4-FixByPragmas-clang-gcc/
````

As for the case we just saw, only the source file is modified. We add here macros to separate clearly gcc and clang cases:

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

And both now works without warnings.

### Fixing clang AND gcc warnings a bit more gracefully

Go in the fifth directory:

````
cd ../../5-FixByPragmas-common/
````

There are two changes here:

* A specific header file has been devised to include properly Boost filesystem. This way, if later you need it somewhere else you may include this file directly and get the right output in just one include line.
* More a matter of taste: I used macro magic to put in common the pragma commands that may work for both gcc and clang.

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````



