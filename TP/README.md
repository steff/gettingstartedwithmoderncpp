# How to use Docker for TP?

First of all, make sure [Docker](https://www.docker.com/) is properly installed and running.

## Getting the image

Then to get the relevant image you may either:

### Build the image from the provided Dockerfile

Type in a terminal (in the same folder this README is):

````
docker build -t tp_formation_cpp .
````

This may take few minutes.

### Or fetching it from the gitlab registry

Type in a terminal (provided you have an account on the [Inria gitlab](https://gitlab.inria.fr) - if not you may create one easily):

````
docker login registry.gitlab.inria.fr
docker pull registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/tp_formation_cpp:latest
````

## Running the image

Run *from the TP directory of the project* the command:

````
docker run -v $PWD:/Codes/TP --cap-drop=all -it tp_formation_cpp
````

For those of you not familiar with Docker:

* `-v` creates a mapping between local folder and the /Codes/ThirdPartyWarning folder in the container; this enables you to edit the file from your comfy environment and see the file edited this way in the Docker container.
* `--cap-drop=all` is a safety when you're running a Docker image not built by yourself: you're essentially blocking the few remaining operations that might impact your own environment that Docker lets by default open with the run command.
* `-it` tells we want an interactive session: your terminal session will place you inside the container (here a Fedora environment).

`tp_formation_cpp` is a Docker **image**; the instantiation of this image obtained after the run command is a **container**. All the modifications you may do in the container won't be kept when you leave it, except for the files that will have been written in your own filesystem thanks to the `-v` option.

