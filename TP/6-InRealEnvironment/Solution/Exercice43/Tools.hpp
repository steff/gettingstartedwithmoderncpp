#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <iostream>
#include <cmath>

#include "Error.hpp"


//! I create here a new struct in charge of doing once and for all the conversion.
template<class IntT>
struct PrintIntHelper
{

    //! std::conditional_t is really close to the ternary operator except that it applies to types.
    using return_type = std::conditional_t
        <
            std::is_same<IntT, char>() || std::is_same<IntT, unsigned char>(),
            int,
            IntT
        >;
            
    static return_type Do(IntT value);


};



enum class RoundToInteger { no, yes };




// From C++ 14 onward
// The function wasn't mandatory per se but allows to a more user-friendly interface:
// PrintInt(value);
// rather than
// PrintIntHelper<IntT>::Do(value);
template<class T>
auto PrintInt(T value);

//! Returns `number` * (2 ^ `exponent`) 
template<class IntT>
IntT times_power_of_2(IntT number, int exponent);


//! Round to `x` the nearest integer.
template<class IntT>
IntT round_as_int(double x);


// Maximum integer that might be represented with `Nbits` bits.
template<class IntT>
IntT max_int(int Nbits);


#include "Tools.hxx"

#endif

