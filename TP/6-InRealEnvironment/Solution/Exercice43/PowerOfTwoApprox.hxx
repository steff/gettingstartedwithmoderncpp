
template<class IntT>
void helper_compute_power_of_2_approx(double value, int exponent, IntT& numerator, IntT& denominator)
{
    constexpr IntT one = static_cast<IntT>(1);
    denominator = times_power_of_2(one, exponent);   
    numerator = round_as_int<IntT>(value * denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::PowerOfTwoApprox(int Nbits, double value)
{
    IntT max_numerator = max_int<IntT>(Nbits);
    
    auto& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    IntT denominator {};
    
    if (value < 0.)
    {
        sign_ = -1;
        value = -value;
    }
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        helper_compute_power_of_2_approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    helper_compute_power_of_2_approx(value, --exponent, numerator, denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::operator double() const
{
    constexpr IntT one = static_cast<IntT>(1);
    IntT denominator = times_power_of_2(one, exponent_);
    return static_cast<double>(numerator_) * sign_ / denominator;
}


template<class IntT>
IntT PowerOfTwoApprox<IntT>::GetNumerator() const
{
    return numerator_;
}


template<class IntT>
int PowerOfTwoApprox<IntT>::GetExponent() const
{
    return exponent_;
}


template<class IntT>
std::string PowerOfTwoApprox<IntT>::GetSignAsCharacter() const
{
    assert(sign_ == 1 || sign_ == -1);
    
    if (sign_ == 1)
        return "";
    else
        return "-";    
}


template<class IntT>
IntT PowerOfTwoApprox<IntT>::GetSignAsNumber() const
{
    return static_cast<IntT>(sign_);
}



template<class IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)
{
    out << approximation.GetSignAsCharacter() << PrintInt(approximation.GetNumerator()) << "/2^" << approximation.GetExponent();
    return out;
}


template<class IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx)
{
    IntT product;
    
    if (__builtin_mul_overflow(approx.GetNumerator(), coefficient, &product))
        throw Error("Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

    return approx.GetSignAsNumber() * times_power_of_2(product, -approx.GetExponent());
}
 

template<class IntT> 
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient)
{
    return coefficient * approx;
}
